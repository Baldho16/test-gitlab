package com.mit.proyecto1.controller;

@RestController
public class Proyecto1Controller {

	
	@Autowired
	Proyecto1Service proyecto1Service;
	
	@PostMapping(path="/login")
	public String void login(@RequestBody String usuario) {	
		return proyecto1Service.login(usuario);
	}
	
	@PostMapping(path="/resetpwd")  
	public String resetPwd(String usuario) {	
		return proyecto1Service.resetPwd(usuario);
	}

}
